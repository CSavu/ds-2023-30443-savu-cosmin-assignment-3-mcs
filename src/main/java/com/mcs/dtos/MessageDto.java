package com.mcs.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mcs.entities.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MessageDto {
    private String uid;
    @JsonProperty("isTyping")
    private boolean isTyping;
    private boolean read;
    private Long timestamp;
    private String message;
    private Integer senderUserId;
    private String senderUsername;
    private Role senderRole;
    private Integer receiverUserId;
}
