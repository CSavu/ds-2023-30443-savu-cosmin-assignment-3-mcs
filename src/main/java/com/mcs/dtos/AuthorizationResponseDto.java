package com.mcs.dtos;

import com.mcs.entities.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AuthorizationResponseDto {
    private boolean isTokenValid;
    private Integer userId;
    private String username;
    private Role role;
}
