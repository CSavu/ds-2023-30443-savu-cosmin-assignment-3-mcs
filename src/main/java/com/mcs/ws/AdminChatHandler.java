package com.mcs.ws;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mcs.adapters.UserManagementServiceAdapter;
import com.mcs.dtos.MessageDto;
import com.mcs.dtos.UserDto;
import com.mcs.entities.Role;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.server.WebSession;
import org.springframework.web.socket.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Component
public class AdminChatHandler implements WebSocketHandler {

    private final UserManagementServiceAdapter umsAdapter;
    private final ObjectMapper objectMapper;

    @Autowired
    public AdminChatHandler(UserManagementServiceAdapter umsAdapter) {
        this.umsAdapter = umsAdapter;
        objectMapper = new ObjectMapper();
    }

    // Map to store the association between session IDs and userIds
    private static final Map<UserDto, WebSocketSession> regularSessionUserMap = new HashMap<>();
    private static final Map<UserDto, WebSocketSession> adminSessionUserMap = new HashMap<>();

    @Override
    public void afterConnectionEstablished(WebSocketSession webSocketSession) throws IOException {
        String userId = extractUserId(webSocketSession);
        if (userId != null) {
            log.info("Connection established with session id={} and userId={}", webSocketSession.getId(), userId);

            UserDto user = umsAdapter.getById(Integer.parseInt(userId));

            if (user != null && user.getRole() != null) {
                if (user.getRole() == Role.REGULAR_USER) {
                    regularSessionUserMap.put(user, webSocketSession);
                } else if (user.getRole() == Role.ADMIN) {
                    adminSessionUserMap.put(user, webSocketSession);
                }
            } else {
                log.error("User with userId={} not found", userId);
                webSocketSession.close();
            }
        }
    }

    @Override
    public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage) throws Exception {
        log.info("Message received from session id={}: {}", webSocketSession.getId(), webSocketMessage.getPayload());

        MessageDto message = objectMapper.readValue(webSocketMessage.getPayload().toString(), MessageDto.class);

        // Send message to all admin users if the message is from a regular user
        if (message.getSenderUserId() != null && adminSessionUserMap.keySet().stream().noneMatch(user -> user.getId().equals(message.getSenderUserId()))) {
            adminSessionUserMap.values().forEach(session -> {
                try {
                    if (session.isOpen()) {
                        UserDto user = regularSessionUserMap.keySet().stream().filter(us -> us.getId().equals(message.getSenderUserId())).findFirst().orElse(null);
                        if (user != null) {
                            message.setSenderUsername(user.getUsername());
                            message.setSenderRole(user.getRole());
                            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(message)));
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } else if (message.getSenderUserId() != null && adminSessionUserMap.keySet().stream().anyMatch(user -> user.getId().equals(message.getSenderUserId()))) {
            // Send message to the regular user if the message is from an admin user
            try {
                List<UserDto> users = regularSessionUserMap.keySet().stream().filter(user -> user.getId().equals(message.getReceiverUserId())).collect(Collectors.toList());
                List<WebSocketSession> openRegularUserSessions = users.stream().map(regularSessionUserMap::get).filter(WebSocketSession::isOpen).collect(Collectors.toList());

                openRegularUserSessions.forEach(session -> {
                    try {
                        session.sendMessage(new TextMessage(objectMapper.writeValueAsString(message)));
                    } catch (IOException e) {
                        log.error("Error occurred while sending message to userId={}", message.getReceiverUserId(), e);
                    }
                });
            } catch (Exception e) {
                log.error("Error occurred while sending message to userId={}", message.getReceiverUserId(), e);
            }
        }
    }

    @Override
    public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable) {
        log.error("Error occurred in session id={}", webSocketSession.getId(), throwable);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession webSocketSession, CloseStatus closeStatus) {
        log.info("Connection closed with session id={}", webSocketSession.getId());
    }

    public void sendMessageToUser(String userId, String message) {
        WebSocketSession session = regularSessionUserMap.get(
                regularSessionUserMap.keySet().stream().filter(user -> user.getId().equals(Integer.parseInt(userId))).findFirst().orElse(null)
        );
        if (session != null && session.isOpen()) {
            try {
                log.info("Sending message to userId={}", userId);
                session.sendMessage(new TextMessage(message));
            } catch (Exception e) {
                log.error("Error occurred while sending message to userId={}", userId, e);
            }
        }
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }

    private String extractUserId(WebSocketSession webSocketSession) {
        // Example: extracting userId from query parameters
        String userId = webSocketSession.getUri().getQuery();
        if (userId != null && userId.startsWith("userId=")) {
            return userId.substring("userId=".length());
        }
        return null;
    }
}
