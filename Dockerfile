FROM openjdk:17.0.1
VOLUME /tmp
EXPOSE 8446
ARG JAR_FILE
COPY ${JAR_FILE} mcs.jar
ENTRYPOINT ["java","-Dspring.profiles.active=docker","-jar","/mcs.jar"]